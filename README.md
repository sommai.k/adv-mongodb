## Clone command

```
git clone https://gitlab.com/sommai.k/adv-mongodb

cd adv-mongodb

docker-compose up -d
```

## insert orders

```
db.orders.insert([
   { "_id" : 1, "item" : "almonds", "price" : 12, "quantity" : 2 },
   { "_id" : 2, "item" : "pecans", "price" : 20, "quantity" : 1 },
   { "_id" : 3  }
])
```

## insert inventory

```
db.inventory.insert([
   { "_id" : 1, "sku" : "almonds", "description": "product 1", "instock" : 120 },
   { "_id" : 2, "sku" : "bread", "description": "product 2", "instock" : 80 },
   { "_id" : 3, "sku" : "cashews", "description": "product 3", "instock" : 60 },
   { "_id" : 4, "sku" : "pecans", "description": "product 4", "instock" : 70 },
   { "_id" : 5, "sku": null, "description": "Incomplete" },
   { "_id" : 6 }
])
```

## lookup

```
db.orders.aggregate([
   {
     $lookup:
       {
         from: "inventory",
         localField: "item",
         foreignField: "sku",
         as: "inventory_docs"
       }
  }
])
```

## Map Reduce

```
db.orders.mapReduce(
    function() {
        emit(this.item, this.price*this.quantity);
     },
     function(key, values) {
         Array.sum(values)
     },
     {
        query: { quantity: {$gt: 0} },
        out: "order_totals"
     }
 )
```

## generate module, controller, service

```
nest generate module order
nest generate controller order
nest generate service order
```

## install swagger

```
npm i --save @nestjs/swagger swagger-ui-express
```

## Apollo

```
npm i --save @nestjs/graphql
npm i --save @nestjs/apollo
npm i --save graphql
npm i --save apollo-server-express
```

## Generate Resolver

```
nest generate resolver order
```
