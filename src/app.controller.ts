import {
  Body,
  Controller,
  Get,
  Logger,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller('app')
export class AppController {
  logger: Logger = new Logger(AppController.name);

  constructor(private readonly appService: AppService) {}

  @Get('hi')
  getHello(@Query('name') name: string): string {
    // return this.appService.getHello();
    return 'This is my Simple World ' + name;
  }

  @Post('new')
  newHello(@Body() body: AppDTO, @Query('id') id: number) {
    this.logger.log(body);
    this.logger.log(id);
    return 'Success ' + body.code;
  }

  @Put('newArr')
  newArr(@Body() arr: AppDTO[]) {
    arr.map((val) => {
      this.logger.log(val);
    });

    return 'Success';
  }
}

interface AppDTO {
  code: string;
  name: string;
}

// npm start
// npm run start:dev
// npm i --save @nestjs/mongoose mongoose
