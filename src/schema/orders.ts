import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Schema({ collection: 'orders' })
export class Order {
  @Prop()
  item: string;

  @Prop()
  price: number;

  @Prop()
  quantity: number;
}

export type OrderDocument = Order & Document;
export const OrderSchema = SchemaFactory.createForClass(Order);
