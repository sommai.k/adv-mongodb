import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class OrderModel {
  @Field({ nullable: true })
  item: string;

  @Field({ nullable: true })
  price: number;

  @Field({ nullable: true })
  quantity: number;
}
