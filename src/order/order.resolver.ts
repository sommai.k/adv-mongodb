import { Logger } from '@nestjs/common';
import { Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { OrderModel } from 'src/model/order.model';
import { OrderService } from './order.service';

@Resolver((of) => OrderModel)
export class OrderResolver {
  logger: Logger = new Logger(OrderModel.name);

  constructor(private service: OrderService) {}

  @Query(() => [OrderModel], { name: 'orders' })
  async resolveQuery() {
    return this.service.findAll();
  }

  @ResolveField('net', () => Number)
  async resolveNet(@Parent() parent: OrderModel) {
    this.logger.log('in resolve net');
    return (parent.price ?? 0) * (parent.quantity ?? 0);
  }
}
