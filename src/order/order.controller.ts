import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { OrderDTO, OrderService } from './order.service';

@Controller('order')
export class OrderController {
  constructor(private service: OrderService) {}

  @Get()
  findAll() {
    return this.service.findAll();
  }

  @Get('by-item')
  findWithCriteria(@Query('item') item: string) {
    return this.service.findByItem(item);
  }

  @Post()
  creatNew(@Body() body: OrderDTO) {
    return this.service.createNewOrder(body);
  }

  @Delete()
  deleteById(@Query('id') id: string) {
    return this.service.deleteOrder(id);
  }

  @Put()
  updateById(@Query('id') id: string, @Body() body: OrderDTO) {
    return this.service.updateNew(id, body);
  }

  @Put('old')
  updateByIdOld(@Query('id') id: string, @Body() body: OrderDTO) {
    return this.service.updateOrder(id, body);
  }
}
