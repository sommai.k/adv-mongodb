import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Order, OrderDocument } from 'src/schema/orders';
import { Model } from 'mongoose';
import { Types as MongooseTypes } from 'mongoose';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name) private orderModel: Model<OrderDocument>,
  ) {}

  async findAll(): Promise<Order[]> {
    return this.orderModel.find().exec();
  }

  async findByItem(item: string) {
    return this.orderModel.find({ item }).exec();
  }

  async createNewOrder(dto: OrderDTO) {
    const order = new this.orderModel(dto);
    return order.save();
  }

  async deleteOrder(id: string) {
    return this.orderModel.findByIdAndDelete(id);
  }

  async updateOrder(id: string, dto: OrderDTO) {
    const _id = new MongooseTypes.ObjectId(id);
    return this.orderModel.findOneAndUpdate({ _id }, dto);
  }

  async updateNew(id: string, dto: OrderDTO) {
    return this.orderModel.findByIdAndUpdate(id, dto);
  }
}

export interface OrderDTO {
  item: string;
  price: number;
  quantity: number;
}
